#!/usr/bin/env sh
# (C) Copyright Ikmal Ahmad
# All Rights Reserved
#
# FILE  : priv.sh
# DESC  : Implement priviledge credential for user. User creation is not covered.
#       : Script have to run as root.
# EXIT CODES:
#   1 - Command run as non-root
#   2 - User is not exist
#   3 - Priviledge file is not exist
#   4 - OS is not supported
# vi Parameters
# vi Parameters
# Tabwidth: 4
# shiftwidth: 4
#
# SVN Id String: $Id: priv.sh 5774 2020-10-01 14:36:01Z ikmal $ 
# Author            : Ikmal Ahmad
# Modifier's info   :
# Name      Date        Info
# Ikmal     20201001    - Initial version
# Ikmal     20201010    - Make it modular
PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/local/soe/sbin:\
/usr/local/soe/bin:/usr/local/Utility/bin:/usr/sfw/sbin:/usr/sfw/bin:/opt/OV/contrib:/usr/contrib/bin"
export PATH

# Script Version
pVer=20201012
SvsReq='SR_Number'

# Global Variables
DEBUG=0
DATESTAMP="`date +%d%m%Y`"          # Date
HOSTNAME=`uname -n`                 # Hostname
NULL="/dev/null"                    # Null
OSNAME=`uname`                      # OS type
Proj="BMC Discovery Project ($SvsReq)"
HeTop="# Define for $Proj"          # Output Header
HeEnd="# End of $Proj"              # Output Footer
PwdFl='/etc/passwd'
PrFile='/etc/sudoers'
UserID='bmcdisc'
GrDesc='bmcdiscovery'

# Platform
case "$OSNAME" in
    SunOS)
        # Solaris
        UID_Cmd=`/usr/xpg4/bin/id -u`
        # Solaris version
        OSVER="`uname -r | cut -d'.' -f2`"
        # Priviledge
        PrPrA='/etc/security/prof_attr'
        PrPrADf='bmcdiscovery:::This profile for bmcdiscovery to execute'
        PrExA='/etc/security/exec_attr'
        PrExADf='bmcdiscovery:suser:cmd:::*:uid=0;privs=proc_owner'
        PrUsA='/etc/user_attr'
        PrUsADf1='bmcdrole::::type=role;profiles=bmcdiscovery'
        PrUsADf2='rbacusr::::type=normal;roles=bmcdrole'
        ;;
    Linux)
        # Linux
        UID_Cmd=`id -u`
        # Priviledge
        PrivCmd='Cmnd_Alias ADDM_LX = /usr/sbin/lpfc/lputil *, /usr/sbin/lpfc/lputil fwlist *, /usr/sbin/hbanywhere/hbacmd *, /usr/sbin/hbanywhere/hbacmd ListHBAs, /usr/sbin/dmidecode, /usr/sbin/hwinfo --bios, /usr/sbin/ss *, /sbin/fdisk -l, /sbin/mii-tool -v *, /sbin/ethtool *, /usr/sbin/ethtool *, /sbin/hdparm -l *, /sbin/lsmod, /sbin/lvs *, /usr/sbin/lvs *, /usr/sbin/pvs *, /sbin/pvs *, /usr/sbin/vgs *, /sbin/vgs *,/usr/sbin/pvdisplay *, /sbin/pvdisplay *, /usr/sbin/vgdisplay *, /sbin/vgdisplay *, /usr/sbin/lvdisplay *, /sbin/lvdisplay *, /usr/bin/pmap, /sbin/multipath -ll, /sbin/scsi_id -g *, /sbin/powermt, /usr/bin/test -x * -a -r *, /usr/bin/test -d *, /usr/bin/test -d * -a -r *, /usr/bin/test -f * -a -r *, /usr/bin/test -f *, /usr/bin/test -x *, /usr/bin/test *, /bin/ls, /usr/bin/ls, /bin/df *, /usr/sysv/bin/df *, /bin/netstat, /usr/sbin/ifconfig -a, /sbin/ifconfig -a, /usr/bin/ps *, /bin/ps *, /bin/grep, /bin/egrep, /usr/bin/grep, /usr/bin/egrep, /usr/sbin/lsof *, /bin/cat *, /usr/bin/cat *'
        PrivUsr='User_Alias ADDMSVC = bmcdisc'
        PrivDev='ADDMSVC ALL = NOPASSWD: ADDM_LX'
        ;;
    AIX)
        # AIX
        UID_Cmd=`id -lu`
        # Priviledge
        PrivCmd='Cmnd_Alias ADDM_IBM = /usr/sbin/lswpar *, /usr/sbin/lslpp *, /usr/sbin/powermt dev=all, /usr/sbin/powermt display dev=all, /usr/sbin/wlmstat, /usr/sbin/smtctl, /usr/sbin/lscfg, /usr/bin/svmon, /usr/sbin/svmon, /usr/sbin/lspv *, /sbin/lspv *, /usr/sbin/lsvg *, /sbin/lsvg *, /usr/sbin/lpfc/lputil *, /usr/sbin/lpfc/lputil fwlist *, /usr/bin/test -x * -a -r *, /usr/bin/test -x * -a -r *, /usr/bin/test -d *, /usr/bin/test -d * -a -r *, /usr/bin/test -f * -a -r *, /usr/bin/test -f *, /usr/bin/test -x *, /usr/bin/test *, /bin/ls, /usr/bin/ls, /bin/df *, /usr/sysv/bin/df *, /bin/netstat, /usr/sbin/ifconfig -a, /sbin/ifconfig -a, /usr/bin/ps *, /bin/ps *, /bin/grep, /bin/egrep, /usr/bin/grep, /usr/bin/egrep, /usr/sbin/lsof *, /bin/cat *, /usr/bin/cat *'
        PrivUsr='User_Alias ADDMSVC = bmcdisc'
        PrivDev='ADDMSVC ALL = NOPASSWD: ADDM_IBM'
        ;;
#    HP-UX)
#        # HP-UX
#        export UNIX95=1
#        UID_Cmd=`id -u`
#        ;;
    *)
        printf "\nOS not supported\n"
        exit 4
        ;;
esac

# How-to/Header
USAGE="Usage :\n\t$0\n"

# Modules
# Function          : bkp_head()
bkp_head()
{
    # Usage         : Backup file & add Header,
    # parameters    : none
    # returns       : none
    if [ ! -f $BkpFl ]; then
        printf "\nCreating backup for $PrFile\n\t=> Backup Filename\t: $BkpFl\n"
        cp -dpR $PrFile $BkpFl
    fi

    # Add Header
    He=`grep -i "$HeTop" $PrFile`
    HeSt=$?

    if [ $HeSt -ne 0 ]; then
        printf "\n#\n$HeTop\n#\n" >> $PrFile
    fi
}

# Function          : footer()
footer()
{
    # Usage         : Add Footer
    # parameters    : none
    # returns       : none
    Fo=`grep -i "$HeEnd" $PrFile`
    FoSt=$?

    if [ $FoSt -ne 0 ]; then
        printf "\n#\n$HeEnd\n#" >> $PrFile
    fi
}

# Function          : test_user()
test_user()
{
    # Usage         : check if user is root
    # parameters    : none
    # returns       : returns code
    # returns code  : 0 - root
    #                 1 - non-root
    if [ $UID_Cmd != 0 ]; then
        printf "Not logged in as root. Exitting...\n"
        exit 1
    fi
}

# Function          : check_user()
check_user()
{
    # Usage         : check if user is exist
    # parameters    : none
    # returns       : returns code
    # returns code  : 0 - user exists
    #                 2 - user not exists
    # Checking user, if not exist exit.  Request to create it.
    GetUsr=`grep $UserID $PwdFl`
    GetUsrSt=$?

    if [ $GetUsrSt -ne 0 ]; then
        echo "ERROR: $UserID is not exist.  Please create $UserID before running this script."
        exit 2
    fi
}

# Function          : add_def()
add_def()
{
    # Usage         : Add the definition.
    # parameters    : none
    # returns       : none
    bkp_head

    # Add definition
    printf "\n$PrDef\n" >> $PrFile
}

# Function          : no_file()
no_file()
{
    # Usage         : File not exists
    # parameters    : none
    # returns       : none
    echo "File $PrFile is not exist"
    exit 3
}

# Main
# Check running as root
test_user
check_user

# Checking if definition is in sudoers (Linux & AIX)
if [ "$OSNAME" = "AIX" ] || [ "$OSNAME" = "Linux" ] ; then
    # Check if sudoers file is exist
    if [ -f $PrFile ]; then
        # Check Cmnd_Alias Definition is exist
        PrDef=$PrivCmd

        CmdAl=`grep -i "$PrDef" $PrFile`
        CmdAlSt=$?

        if [ $CmdAlSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrFile-$SvsReq-$DATESTAMP"

            add_def
        fi

        # Check if User_Alias Definition is exist
        PrDef=$PrivUsr
        PrUsr=`grep -i "$PrDef" $PrFile`
        PrUsrSt=$?

        if [ $PrUsrSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrFile-$SvsReq-$DATESTAMP"

            add_def
        fi

        # Check if Auth Definition is exist
        PrDef=$PrivDev
        PrADf=`grep -i "$PrDef" $PrFile`
        PrADfSt=$?

        if [ $PrADfSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrFile-$SvsReq-$DATESTAMP"

            add_def
        fi
        footer
    else
        printf "Please make sure sudoers package is installed.\n"
        no_file
    fi

# Checking if definition is in rbac (Solaris)
elif [ "$OSNAME" = "SunOS" ]; then
    # Check if file is exist
    PrFile=$PrPrA
    PrDef=$PrPrADf
    if [ -f $PrPrA ]; then
        # Check profile attribute is exist
        PrPAl=`grep -i "$PrDef" $PrFile`
        PrPAlSt=$?

        if [ $PrPAlSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrPrA-$SvsReq-$DATESTAMP"

            add_def
            footer
        fi
    else
        no_file
    fi

    PrFile=$PrExA
    PrDef=$PrExADf
    if [ -f $PrExA ]; then
        # Check execute attribute is exist
        PrEAl=`grep -i "$PrDef" $PrFile`
        PrEAlSt=$?

        if [ $PrEAlSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrExA-$SvsReq-$DATESTAMP"

            add_def
            footer
        fi
    else
        no_file
    fi
    
    PrFile=$PrUsA
    if [ -f $PrUsA ]; then
        # Check profile Definition is exist
        PrUAl=`egrep -i "$PrUsADf1|$PrUsADf2" $PrFile`
        PrUAlSt=$?

        if [ $PrUAlSt -ne 0 ]; then
            # Backup file if there is an update.
            BkpFl="$PrUsA-$SvsReq-$DATESTAMP"

            bkp_head
            # Add definition
            printf "\n$PrUsADf1\n$PrUsADf2\n" >> $PrFile
            footer
        fi
    else
        no_file
    fi
fi
