From: alex tan
Sent: Tuesday, 2 February, 2021 3:15 PM
To: Mohamad Siddique Bin N Abdul Rahim; Khor Joe Jack; Calvin Kok Wai Tim; Kanchi Ranganath; Shanmuganathan a/l Maganathan
Cc: Chin Loong Tuck; ikmal ahmad; soon sheng lee
Subject: RE: BMC-Clientele integration


Hi Joe Jack /Calvin

After looking into the integration data requirement, below field are only available in Discovery but not CMDB
1.	Disk Available
2.	Kernel level
3.	Firmware version
4.	Hardware end of support date
5.	Hardware end of extended support date
6.	Network connectivity relationship 

To allowed this data available in CMDB and proceed development of the integration from CMDB, it will required a change request on BMC discovery and CMDB to allowed those data available in CMDB

To avoid to have a change request, we are proposing a different approach for this integration, to build a customized query in Discovery to extract the data into 2 file, for clientele to load into database
1.	Assets properties file(Discovery Assets Detail.xlxs) – will have all the asset information detail except network relationship 
2.	Assets network relationship file（assets network relationship.xlxs） – will have network relationship information for all assets

For Assets properties file(3090 assets) it take < 1 sec  and for Assets network relationship file(512924 relationships) it took 1+ sec to completed, 
1.	Please do review the both file is ok Discovery Assets Detail
2.	If the field and content is ok, please do test to load into Clientele, for UAT purpose, and do lets us know the result 

With this speed of extraction, do you think it would be sufficient to handle manual integration between BMC and Clientele,  as the data update only happen once a week based on BCM weekly scanning schedule 

Regards
Alex Tan
