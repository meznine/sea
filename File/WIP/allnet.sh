#!/bin/sh

now=$(date)

IFS=' '

echo "Date/Time;Endpoint;Port;Status;Description"

for i in 192.168.0.61
do
for j in 4 {21..25} 53 80 135 389 443 513 636 902 1433 1521 3306 4100 5988 5989 15003 15004
do

 result=$(nmap -sT -p $j $i | grep "tcp")

 #read -a strarr <<< "$result"
 #echo "There have ${#strarr[*]} words."

 echo "$now;$i;${result// /;}"

done

for x in 4 53 123 161 389 636 3306
do

 result=$(nmap -sU -p $x $i | grep "udp")

 echo "$now;$i;${result// /;}"

done

done
