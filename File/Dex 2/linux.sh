#!/bin/sh

# Simple script to obtain host info from Linux systems
# Script is divided into sections to match discovery methods

os=`uname -s`
if [ "$os" != "Linux" ]; then
    echo This script must be run on Linux
    exit 1
fi

# Set PATH
PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Initialisation
tw_locale=`locale -a | grep -i en_us | grep -i "utf.*8" | head -n 1 2>/dev/null`

LANGUAGE=""
if [ "$tw_locale" != "" ]; then
    LANG=$tw_locale
    LC_ALL=$tw_locale
else
    LANG=C
    LC_ALL=C
fi
export LANG LC_ALL

# bash 4 added command_not_found_handle called when a command is unavailable.
# PackageKit-command-not-found RHEL and Fedora ask to install the package
# making discovery stall. Undefine function to stop this.
unset command_not_found_handle


# Stop alias commands changing behaviour.
unalias -a

# Insulate against systems with -u set by default.
set +u

if [ -w /tmp ] 
then
    # use a /tmp file to capture stderr
    TW_CAPTURE_FILE=/tmp/tideway_status_$$
    export TW_CAPTURE_FILE
    rm -f $TW_CAPTURE_FILE

    tw_capture(){
        TW_NAME=$1
        shift
        echo begin cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE
        "$@" 2>>$TW_CAPTURE_FILE
        RETURN_VAL=$?
        echo end cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE

        echo cmd_status_$TW_NAME=$RETURN_VAL >>$TW_CAPTURE_FILE
        return $RETURN_VAL
    }

    tw_report(){
        if [ -f $TW_CAPTURE_FILE ]
        then 
            cat $TW_CAPTURE_FILE 2>/dev/null
            rm -f $TW_CAPTURE_FILE 2>/dev/null
        fi
    }
else
    # can't write to /tmp - do not capture anything
    tw_capture(){
        shift
        "$@" 2>/dev/null
    }

    tw_report(){
        echo "cmd_status_err_status_unavailable=Unable to write to /tmp"
    }
fi

############################################################################
# Function to detect the cloud platform, if any

tw_detect_cloud_platform() {
    # Have we already detected the cloud platform?

    if [ -n "$TW_CLOUD_PLATFORM" ]; then
        return 0
    fi

    export TW_CLOUD_PLATFORM=
    export TW_CLOUD_ID=
    export TW_CLOUD_IMDS_CMD=

    # We need curl to make the required requests

    if [ ! -x /usr/bin/curl ]; then
        return 1
    fi

    # Trying to detect Amazon EC2

    # These web requests are not chargeable.
    # See: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html

    # Try IMDSv2 first
    TOKEN=`curl --connect-timeout 5 --max-time 10 --fail -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 1800" 2>/dev/null`
    if [ ! -z "$TOKEN" ]; then
        TW_CLOUD_IMDS_CMD="curl --connect-timeout 5 --max-time 10 --fail -H \"X-aws-ec2-metadata-token: $TOKEN\" http://169.254.169.254/latest/meta-data"
    else
        # Try IMDSv1
        TW_CLOUD_IMDS_CMD="curl --connect-timeout 5 --max-time 10 --fail http://169.254.169.254/latest/meta-data"
    fi

    TW_CLOUD_ID=`$TW_CLOUD_IMDS_CMD/instance-id 2>/dev/null | egrep '^i-[0-9a-f]+$'`
    if [ -n "$TW_CLOUD_ID" ]; then
        # We need to check for OpenStack EC2-compatible metadata
        # https://docs.openstack.org/nova/latest/user/metadata.html#metadata-ec2-format
        OPENSTACK=`curl --connect-timeout 5 --max-time 10 --fail http://169.254.169.254/openstack 2>/dev/null`
        if [ ! -z "$OPENSTACK" ]; then
            TW_CLOUD_PLATFORM=OPENSTACK
            TW_CLOUD_IMDS_CMD="curl --connect-timeout 5 --max-time 10 --fail http://169.254.169.254/openstack/latest"
            return 0
        fi

        # Cloud platform is AWS
        TW_CLOUD_PLATFORM=AWS
        return 0
    fi

    # Try Azure
    # see: https://docs.microsoft.com/en-us/azure/virtual-machines/linux/instance-metadata-service
    TW_CLOUD_ID=`curl --connect-timeout 5 --max-time 10 --fail -H 'Metadata:true' "http://169.254.169.254/metadata/instance/compute/vmId?api-version=2019-06-01&format=text" 2>/dev/null`
    if [ -n "$TW_CLOUD_ID" ]; then
        TW_CLOUD_PLATFORM=AZURE
        TW_CLOUD_IMDS_CMD="curl --connect-timeout 5 --max-time 10 --fail -H 'Metadata:true' http://169.254.169.254/metadata"
        return 0
    fi

    # Try Google Compute engine
    # see: https://cloud.google.com/compute/docs/storing-retrieving-metadata
    TW_CLOUD_ID=`curl --connect-timeout 5 --max-time 10 --fail --header 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/id 2>/dev/null`
    if [ -n "$TW_CLOUD_ID" ]; then
        TW_CLOUD_PLATFORM=GCE
        TW_CLOUD_IMDS_CMD="curl --connect-timeout 5 --max-time 10 --fail --header 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1"
        return 0
    fi

    TW_CLOUD_PLATFORM=None
    return 1
}

## BEGIN PRIVILEGE FUNCTIONS ###############################################
# Privilege command   : None
# AWS Systems Manager : True
############################################################################

# AWS System Manager privilege elevation functions
if [ `whoami` = "ssm-user" ]; then

    # cat requires superuser privileges to read some files belonging to
    # other users
    PRIV_CAT() {
        sudo cat "$@"
    }
    # df requires superuser privileges to report file systems owned by other
    # users
    PRIV_DF() {
        sudo "$@"
    }
    # dmidecode requires superuser privileges to read data from the system
    # BIOS
    PRIV_DMIDECODE() {
        sudo "$@"
    }
    # VMware's esxcfg-info command requires superuser privileges
    PRIV_ESXCFG() {
        sudo "$@"
    }
    # ethtool requires superuser privileges to display any interface speed
    # and negotiation settings
    PRIV_ETHTOOL() {
        sudo "$@"
    }
    # hbacmd requires superuser privileges to display any HBA information
    PRIV_HBACMD() {
        sudo "$@"
    }
    # hwinfo requires superuser privileges to read data from the system BIOS
    PRIV_HWINFO() {
        sudo "$@"
    }
    # lputil requires superuser privileges to display any HBA information
    PRIV_LPUTIL() {
        sudo "$@"
    }
    # ls requires superuser privileges to allow access to some directories
    # owned by other users
    PRIV_LS() {
        sudo ls "$@"
    }
    # lsof requires superuser privileges to display information on processes
    # other than those running as the current user
    PRIV_LSOF() {
        sudo "$@"
    }
    # mii-tool requires superuser privileges to display any interface speed
    # and negotiation settings
    PRIV_MIITOOL() {
        sudo "$@"
    }
    # netstat requires superuser privileges to display process identifiers
    # (PIDs) for ports opened by processes not running as the current user
    PRIV_NETSTAT() {
        sudo "$@"
    }
    # Allow patterns to run commands with elevated privileges
    PRIV_RUNCMD() {
        sudo "$@"
    }
    # ss requires superuser privileges to display process identifiers (PIDs)
    # for ports opened by processes not running as the current user
    PRIV_SS() {
        sudo "$@"
    }
    # test requires superuser privileges to allow access to some files owned
    # by other users
    PRIV_TEST() {
        sudo test "$@"
    }
    # Xen's xe command requires superuser privileges
    PRIV_XE() {
        sudo "$@"
    }

else

    # Default privilege elevation functions

    # cat requires superuser privileges to read some files belonging to
    # other users
    PRIV_CAT() {
        cat "$@"
    }
    # df requires superuser privileges to report file systems owned by other
    # users
    PRIV_DF() {
        "$@"
    }
    # dmidecode requires superuser privileges to read data from the system
    # BIOS
    PRIV_DMIDECODE() {
        "$@"
    }
    # VMware's esxcfg-info command requires superuser privileges
    PRIV_ESXCFG() {
        "$@"
    }
    # ethtool requires superuser privileges to display any interface speed
    # and negotiation settings
    PRIV_ETHTOOL() {
        "$@"
    }
    # hbacmd requires superuser privileges to display any HBA information
    PRIV_HBACMD() {
        "$@"
    }
    # hwinfo requires superuser privileges to read data from the system BIOS
    PRIV_HWINFO() {
        "$@"
    }
    # lputil requires superuser privileges to display any HBA information
    PRIV_LPUTIL() {
        "$@"
    }
    # ls requires superuser privileges to allow access to some directories
    # owned by other users
    PRIV_LS() {
        ls "$@"
    }
    # lsof requires superuser privileges to display information on processes
    # other than those running as the current user
    PRIV_LSOF() {
        "$@"
    }
    # mii-tool requires superuser privileges to display any interface speed
    # and negotiation settings
    PRIV_MIITOOL() {
        "$@"
    }
    # netstat requires superuser privileges to display process identifiers
    # (PIDs) for ports opened by processes not running as the current user
    PRIV_NETSTAT() {
        "$@"
    }
    # Allow patterns to run commands with elevated privileges
    PRIV_RUNCMD() {
        "$@"
    }
    # ss requires superuser privileges to display process identifiers (PIDs)
    # for ports opened by processes not running as the current user
    PRIV_SS() {
        "$@"
    }
    # test requires superuser privileges to allow access to some files owned
    # by other users
    PRIV_TEST() {
        test "$@"
    }
    # Xen's xe command requires superuser privileges
    PRIV_XE() {
        "$@"
    }

fi


# Formatting directive
echo FORMAT Linux

# getDeviceInfo
echo --- START device_info

echo 'hostname:' `hostname 2>/dev/null`
echo 'fqdn:' `hostname --fqdn 2>/dev/null`
dns_domain=`hostname -d 2>/dev/null | sed -e 's/(none)//'`
if [ "$dns_domain" = "" -a -r /etc/resolv.conf ]; then
  dns_domain=`awk '/^(domain|search)/ {sub(/\\\\000$/, "", $2); print $2; exit }' /etc/resolv.conf 2>/dev/null`
fi
echo 'dns_domain: ' $dns_domain

nis_domain=`domainname 2>/dev/null`
if [ "$nis_domain" = "" ]; then
  nis_domain=`hostname -y 2>/dev/null`
fi
echo 'domain: ' $nis_domain | sed -e 's/(none)//'

os=""
# VMware appliances
if [ "$os" = "" ]; then
    type=`cat /etc/vmware/deployment.node.type 2> /dev/null`
    # VMware vCenter Server Appliance
    version=`rpm -q --queryformat "%{VERSION} ${type} build %{RELEASE}" VMware-vpxd 2>/dev/null | grep build`
    if [ "$version" = "" ]; then
        # VMware Platform Service Controller
        version=`rpm -q --queryformat "%{VERSION} ${type} build %{RELEASE}" psc_client 2>/dev/null | grep build`
    fi
    if [ "$version" != "" ]; then
        os="VMware vCenter Server Appliance $version"
    fi
fi
# SuSE lsb_release does not provide service pack so prefer SuSE-release file
# However, this file is being deprecated so we will fallback to os-release
# (see below)
if [ "$os" = "" -a -r /etc/SuSE-release ]; then
    os=`cat /etc/SuSE-release | egrep -v '^#'`
fi
if [ "$os" = "" -a -x /usr/bin/lsb_release ]; then
    # We'd like to use -ds but that puts quotes in the output!
    os=`/usr/bin/lsb_release -d | cut -f2 -d: | sed -e 's/^[ \t]//'`
    if [ "$os" = "(none)" ]; then
        os=""
    elif [ "$os" != "" ]; then
        if [ -f /etc/redhat-release ]; then
            # Check to see if its a variant of Red Hat
            # Early 4/5 version have /etc/enterprise-release without mentioning
            # Oracle in the name.
            if [ -f /etc/enterprise-release ]; then
                os="Oracle $os"
            elif [ -f /etc/oracle-release ]; then
                os=`cat /etc/oracle-release`
            fi
        fi
    fi
fi
if [ "$os" = "" -a -r /proc/vmware/version ]; then
    os=`grep -m1 ESX /proc/vmware/version`
fi
if [ "$os" = "" -a -r /etc/vmware-release ]; then
    os=`grep ESX /etc/vmware-release`
fi
if [ "$os" = "" -a -r /etc/redhat-release ]; then
    os=`cat /etc/redhat-release`

    # Check to see if its a variant of Red Hat
    # Early 4/5 version have /etc/enterprise-release rather then
    # /etc/oracle-release without mentioning Oracle.
    if [ -f /etc/enterprise-release ]; then
        os="Oracle $os"
    elif [ -f /etc/oracle-release ]; then
        os=`cat /etc/oracle-release`
    fi
fi
if [ "$os" = "" -a -r /etc/debian_version ]; then
    ver=`cat /etc/debian_version`
    os="Debian Linux $ver"
fi
if [ "$os" = "" -a -r /etc/mandrake-release ]; then
    os=`cat /etc/mandrake-release`
fi
if [ "$os" = "" -a -r /etc/os-release ]; then
    # Use os-release for SuSE (if SuSE-release wasn't present, above) and Container Linux
    os_id=`egrep ^ID= /etc/os-release | cut -s -f2 '-d"'`
    if [ "$os_id" = "" ]; then
        os_id=`egrep ^ID= /etc/os-release | cut -f2 -d=`
    fi
    if [ "$os_id" = "sles" -o "$os_id" = "opensuse" -o "$os_id" = "sle_hpc" -o "$os_id" = "coreos" ]; then
        os=`egrep ^PRETTY_NAME /etc/os-release | cut -f2 '-d"'`
    fi
fi
if [ "$os" = "" -a -r /etc/system-release ]; then
    os=`cat /etc/system-release`
fi
if [ "$os" = "" ]; then
    os=`uname -sr 2>/dev/null`
fi
echo 'os:' $os
echo 'os_arch:' `uname -m 2>/dev/null`

echo --- END device_info

# getHostInfo
echo --- START host_info

# According to the dmidecode source code the physical RAM size
# can be given in any unit, from bytes to ZB.
# [https://github.com/mirror/dmidecode/blob/master/dmidecode.c, dmi_print_memory_size()]
# ('KB' stays in to support older versions)
if [ -f /usr/sbin/dmidecode ]; then
    dmidecode_ram=`PRIV_DMIDECODE /usr/sbin/dmidecode 2>/dev/null | awk '
        BEGIN {
            powers["bytes"] = -1
            powers["kB"]    = 0 # kilobytes
            powers["KB"]    = 0 # Kilobytes
            powers["MB"]    = 1 # Megabytes
            powers["GB"]    = 2 # Gigabytes
            powers["TB"]    = 3 # Terabytes
            powers["PB"]    = 4 # Petabytes
            powers["EB"]    = 5 # Exabytes
            powers["ZB"]    = 6 # Zetabytes
        }
        /DMI type 17,/ { keep = 1; next }
        /^Handle 0x/ { keep = 0 }
        keep && /Size: ([0-9]+) (bytes|([kKMGTPEZ]B))/ {
            size += int($2 * (1024 ^ powers[$3]))
        }
        END {
            print size
        }'`
    if [ "${dmidecode_ram}" != "" ]; then
        ram="${dmidecode_ram}KB"
    fi
fi

logical_ram=`awk '/^MemTotal:/ {print $2 "KB"}' /proc/meminfo 2>/dev/null`

if [ -f /usr/sbin/esxcfg-info ]; then
    # On a VMWare ESX controller, report the *real* hardware information
    file=/tmp/tideway-hw-$$
    uuid=""
    PRIV_ESXCFG /usr/sbin/esxcfg-info --hardware > ${file} 2>/dev/null
    if [ $? -eq 0 ]; then
        ram=`grep "Physical Mem\." ${file} | sed 's/[^0-9]*//g'`B
        # For esx/esxi, we should NOT use memory from dmesg or /proc/meminfo
        # because the values are incorrect
        logical_ram=""
        uuid=`grep "BIOS UUID\." ${file}`
    fi
    rm -f ${file}

    # Get UUID as hostid if possible
    if [ "$uuid" != "" ]; then
        # Process horrid BIOS UUID format :(
        echo "$uuid" | sed -e 's/\./ /g' | awk '{
printf("hostid: ");
for(i = 3 ; i < 19; i++)
{
    printf("%02s", substr($i,3,2));
    if (i == 6 || i == 8 || i == 10 || i == 12) printf("-");
}
printf("\n");
}'
    else
        if [ -r /etc/slp.reg ]; then
            uuid=`grep hardwareUuid /etc/slp.reg | cut -f2 -d= | tr '[:upper:]' '[:lower:]' | sed -e 's/"//g'`
            if [ "${uuid}" != "" ]; then
                echo 'hostid:' ${uuid}
            fi
        fi
    fi
fi

if [ -f /opt/xensource/bin/xe ]; then
    XE=/opt/xensource/bin/xe
    # /proc/meminfo reports incorrectly for Xen domains, use "xe"
    uuid=`PRIV_XE $XE host-list | grep uuid | head -n 1 | cut -f2 -d: | awk '{print $1;}'`
    if [ $? -eq 0 ]; then
        logical_ram=`PRIV_XE $XE host-param-get uuid=$uuid param-name=memory-total`
    fi
fi

echo 'kernel:' `uname -r`

if [ "${ram}" != "" ]; then
    echo 'ram:' ${ram}
fi
if [ "${logical_ram}" != "" ]; then
    echo 'logical_ram:' ${logical_ram}
fi

# Get uptime in days and seconds
uptime | awk '
{
  if ( $4 ~ /day/ ) {
    print "uptime:", $3;
    if ( $6 !~ /user/ ) {
      if ( $6 ~ /min/ ) {
        print "uptimeSeconds: ", ($3 * 86400) + ($5 * 60);
      } else {
        split($5,t,":");
        print "uptimeSeconds: ", ($3 * 86400) + (t[1] * 3600) + (t[2] * 60);
      }
    } else {
      print "uptimeSeconds: ", ($3 * 86400);
    }
  } else {
    print "uptime: 0";
    if ( $4 ~ /min/ ) {
      print "uptimeSeconds: ", ($3 * 60);
    } else {
      split($3,t,":");
      print "uptimeSeconds: ", (t[1] * 3600) + (t[2] * 60);
    }
  }
}'

# zLinux?
if [ -r /proc/sysinfo -a -d /proc/dasd ]; then
    echo "candidate_vendor[]:" `egrep '^Manufacturer:' /proc/sysinfo | awk '{print $2;}'`
    type=`egrep '^Type:' /proc/sysinfo | awk '{print $2;}'`
    model=`egrep '^Model:' /proc/sysinfo | awk '{print $2;}'`
    echo "candidate_model[]: $type-$model"
    echo "zlinux_sequence:" `egrep '^Sequence Code:' /proc/sysinfo | awk '{print $3;}'`
    echo "zlinux_vm_name:" `egrep '^VM00 Name:' /proc/sysinfo | awk '{print $3;}'`
    echo "zlinux_vm_software:" `egrep '^VM00 Control Program:' /proc/sysinfo | awk '{print $4, $5;}'`
fi

# Can we get information from the BIOS? We use lshal if available as that
# requires no superuser permissions but we attempt to run all tools as some
# can return invalid values in some cases. The system will select the "best"
# candidate from the values returned, where "best" is the first non-bogus value
if [ -x /usr/bin/lshal ]; then
    /usr/bin/lshal 2>/dev/null | sed -e 's/(string)$//g' -e "s/'//g" | awk '
    $1 ~ /(smbios\.system|system\.hardware)\.serial/ {
        sub(/.*(smbios\.system|system\.hardware).serial = */, "");
        printf("candidate_serial[]: %s\n", $0);
    }
    $1 ~ /(smbios\.system|system\.hardware)\.uuid/ {
        sub(/.*(smbios\.system|system\.hardware)\.uuid = */, "");
        printf("candidate_uuid[]: %s\n", $0);
    }
    $1 ~ /(smbios\.system|system\.hardware)\.product/ {
        sub(/.*(smbios\.system|system\.hardware)\.product = */, "");
        printf("candidate_model[]: %s\n", $0);
    }
    $1 ~ /system(\.hardware)?\.vendor/ {
        sub(/.*(system|system\.hardware)\.vendor = */, "");
        printf("candidate_vendor[]: %s\n", $0);
    }'
fi
if [ -f /usr/sbin/dmidecode ]; then
    # Azure Virtual Machines report their instance ID via the BIOS UUID
        PRIV_DMIDECODE /usr/sbin/dmidecode 2>/dev/null | sed -n '/DMI type 1,/,/^Handle 0x0/p' | awk '
    $1 ~ /Manufacturer:/ { sub(".*Manufacturer: *", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Vendor:/ { sub(".*Vendor: *", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Product/ && $2 ~ /Name:/ { sub(".*Product Name: *", ""); printf("candidate_model[]: %s\n", $0); }
    $1 ~ /Product:/ { sub(".*Product: *",""); printf("candidate_model[]: %s\n", $0 ); }
    $1 ~ /Serial/ && $2 ~ /Number:/ { sub(".*Serial Number: *", ""); printf("candidate_serial[]: %s\n", $0); }
    $1 ~ /UUID:/ { sub(".*UUID: *", ""); printf( "candidate_uuid[]: %s\n", $0 ); } '
fi
if [ -f /usr/sbin/hwinfo ]; then
        PRIV_HWINFO /usr/sbin/hwinfo --bios 2>/dev/null | sed -n '/System Info:/,/Info:/p' | awk '
    $1 ~ /Manufacturer:/ { sub(".*Manufacturer: *", ""); gsub("\"", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Product:/ { sub(".*Product: *", ""); gsub("\"", ""); printf("candidate_model[]: %s\n", $0); }
    $1 ~ /Serial:/ { sub(".*Serial: *", ""); gsub("\"", ""); printf("candidate_serial[]: %s\n", $0); }
    $1 ~ /UUID:/ { sub(".*UUID: *", ""); gsub("\"", ""); printf("candidate_uuid[]: %s\n", $0); } '
fi
if [ -d /sys/class/dmi/id ]; then
    model=`cat /sys/class/dmi/id/product_name 2>/dev/null`
    vendor=`cat /sys/class/dmi/id/sys_vendor 2>/dev/null`
    if [ -r /etc/system-release ]; then
        grep Amazon /etc/system-release >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            model="Amazon Virtual Machine"
            vendor="Amazon"
        fi
    fi
    echo "candidate_model[]: " "$model"
    echo "candidate_serial[]: " `PRIV_CAT /sys/class/dmi/id/product_serial 2>/dev/null`
    echo "candidate_uuid[]: " `PRIV_CAT /sys/class/dmi/id/product_uuid 2>/dev/null`
    echo "candidate_vendor[]: " "$vendor"
fi

# Cloud instance metadata
tw_detect_cloud_platform
if [ $? -eq 0 ]; then
    if [ "$TW_CLOUD_PLATFORM" == "AWS" ]; then
        echo "aws_instance_id: $TW_CLOUD_ID"
        echo "candidate_model[]: Amazon Virtual Machine"
        echo "candidate_vendor[]: Amazon"

        # Get primary MAC so we can find VPC
        primary_mac=`$TW_CLOUD_IMDS_CMD/mac 2>/dev/null`
        if [ "$primary_mac" != "" ]; then
            scope=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$primary_mac/vpc-id 2>/dev/null | egrep '^vpc-[0-9a-f]+$'`
            if [ "$scope" != "" ]; then
                echo "scope: $scope"
            fi
        fi
    
    elif [ "$TW_CLOUD_PLATFORM" == "AZURE" ]; then
        echo "azure_vm_id: $TW_CLOUD_ID"
        echo "candidate_model[]: Virtual Machine"
        echo "candidate_vendor[]: Microsoft Corporation"
    
    elif [ "$TW_CLOUD_PLATFORM" == "GCE" ]; then
        echo "gce_instance_id: " "$TW_CLOUD_ID"
        echo "candidate_model[]: Google Compute Engine"
        echo "candidate_vendor[]: Google"
    
        # Get project IDs
        numeric_project_id=`$TW_CLOUD_IMDS_CMD/project/numeric-project-id 2>/dev/null`
        project_id=`$TW_CLOUD_IMDS_CMD/project/project-id 2>/dev/null`
    
        # Get VPC network 
        network=`$TW_CLOUD_IMDS_CMD/instance/network-interfaces/0/network 2>/dev/null`
    
        # Build scope from network
        scope=`echo $network | sed -e "s/$numeric_project_id/$project_id/"`
        if [ "$scope" != "" ]; then
            echo "scope: $scope"
        fi
    
    elif [ "$TW_CLOUD_PLATFORM" == "OPENSTACK" ]; then
        echo "openstack_instance_id: $TW_CLOUD_ID"
        echo "candidate_model[]: OpenStack Compute"
    fi
fi
    
# PPC64 LPAR?
if [ -r /proc/ppc64/lparcfg ]; then
    echo begin lparcfg:
    cat /proc/ppc64/lparcfg 2>/dev/null
    echo end lparcfg
fi
# LPAR name?
if [ -r /proc/device-tree/ibm,partition-name ]; then
    echo "lpar_partition_name:" `cat /proc/device-tree/ibm,partition-name`
fi

echo --- END host_info

# getMACAddresses
echo --- START ip_link_mac
ip -d -o link show 2>/dev/null | egrep -v '\bbridge(_slave)?|vxlan|openvswitch(_slave)?|veth\b'

echo --- END ip_link_mac

echo --- START ifconfig_mac
ifconfig -a 2>/dev/null
echo --- END ifconfig_mac

# getIPAddresses
echo --- START ip_addr_ip
ip address show 2>/dev/null

tw_detect_cloud_platform
if [ $? -eq 0 ]; then
    # AWS EC2 public/elastic IPs
    if [ "$TW_CLOUD_PLATFORM" = "AWS" ]; then
        macs=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/ 2>/dev/null | sed -e 's/\///g'`
        if [ ! -z "$macs" ]; then
            for mac in $macs
            do
                n=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/device-number 2>/dev/null`
                id=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/interface-id 2>/dev/null`
                public_ips=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/public-ipv4s 2>/dev/null`

                echo "$n: $id: <UP> mtu 1500 state UP"
                echo "    link/ether $mac brd ff:ff:ff:ff:ff:ff"
                for public_ip in $public_ips
                do
                        echo "    inet $public_ip/0 brd 0.0.0.0 scope tw:internet eth$n"
                done
            done
        fi
    fi
fi

echo --- END ip_addr_ip

echo --- START ifconfig_ip
ifconfig -a 2>/dev/null

tw_detect_cloud_platform
if [ $? -eq 0 ]; then
    # AWS EC2 public/elastic IPs
    if [ "$TW_CLOUD_PLATFORM" = "AWS" ]; then
        macs=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/ 2>/dev/null | sed -e 's/\///g'`
        if [ ! -z "$macs" ]; then
            for mac in $macs
            do
                id=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/interface-id 2>/dev/null`
                public_ips=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/public-ipv4s 2>/dev/null`

                echo "$id: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500"
                for public_ip in $public_ips
                do
                    echo "        inet $public_ip netmask 0.0.0.0 scope tw:internet"
                done
                echo "        ether $mac"
            done
        fi
    fi
fi

echo --- END ifconfig_ip

# getNetworkInterfaces
echo --- START ip_link_if
ip -o link show 2>/dev/null
if [ $? -eq 0 ]; then

tw_detect_cloud_platform
if [ $? -eq 0 ]; then
    # AWS EC2 ENIs
    if [ "$TW_CLOUD_PLATFORM" = "AWS" ]; then
        macs=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/ 2>/dev/null | sed -e 's/\///g'`
        if [ ! -z "$macs" ]; then
            for mac in $macs
            do
                n=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/device-number 2>/dev/null`
                id=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/interface-id 2>/dev/null`
                echo "$n: $id: <UP> link/ether $mac"
            done
        fi
    fi
fi


ETHTOOL=""
if [ -f /sbin/ethtool ]; then
    ETHTOOL=/sbin/ethtool
else
    if [ -f /usr/sbin/ethtool ]; then
        ETHTOOL=/usr/sbin/ethtool
    fi
fi

MIITOOL=""
if [ -f /sbin/mii-tool ]; then
    MIITOOL=/sbin/mii-tool
fi

echo 'begin details:'
for i in `ip -o link show 2>/dev/null | egrep '^[0-9]+:' | awk -F: '{print $2;}'`
do
    if [ -d /sys/class/net/$i ]; then
        echo begin /sys/class/net/$i:
        echo name: $i
        for file in address duplex ifindex speed
        do
            path=/sys/class/net/$i/$file
            if [ -r $path ]; then
                value=`cat $path 2>/dev/null`
                if [ "$value" != "" ]; then
                    echo $file: $value
                fi
            fi
        done

        if [ -d /sys/class/net/$i/bonding ]; then
            # Interface is a bonding master
            echo bonded: True
            for file in mode slaves
            do
                path=/sys/class/net/$i/bonding/$file
                if [ -r $path ]; then
                    value=`cat $path 2>/dev/null`
                    if [ "$value" != "" ]; then
                        echo bonding_$file: $value
                    fi
                fi
            done
        fi
        echo end /sys/class/net/$i:
    fi

    SUCCESS=1
    if [ "$ETHTOOL" != "" ]; then
        echo begin ethtool-$i:
        PRIV_ETHTOOL $ETHTOOL $i 2>/dev/null
        SUCCESS=$?
        echo end ethtool-$i:
    fi
    if [ "$MIITOOL" != "" -a $SUCCESS -ne 0 ]; then
        echo begin mii-tool-$i:
        PRIV_MIITOOL $MIITOOL -v $i 2>/dev/null
        echo end mii-tool-$i:
    fi
done
echo 'end details:'
fi

echo --- END ip_link_if

echo --- START ifconfig_if
ifconfig -a 2>/dev/null
if [ $? -eq 0 ]; then

tw_detect_cloud_platform
if [ $? -eq 0 ]; then
    # AWS EC2 ENIs
    if [ "$TW_CLOUD_PLATFORM" = "AWS" ]; then
        macs=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/ 2>/dev/null | sed -e 's/\///g'`
        if [ ! -z "$macs" ]; then
            for mac in $macs
            do
                n=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/device-number 2>/dev/null`
                id=`$TW_CLOUD_IMDS_CMD/network/interfaces/macs/$mac/interface-id 2>/dev/null`
                echo "$id: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500"
                echo "        ether $mac"
            done
        fi
    fi
fi


ETHTOOL=""
if [ -f /sbin/ethtool ]; then
    ETHTOOL=/sbin/ethtool
else
    if [ -f /usr/sbin/ethtool ]; then
        ETHTOOL=/usr/sbin/ethtool
    fi
fi

MIITOOL=""
if [ -f /sbin/mii-tool ]; then
    MIITOOL=/sbin/mii-tool
fi

echo 'begin details:'
for i in `ifconfig -a 2>/dev/null | egrep '^[a-z]' | awk -F: '{print $1;}'`
do
    if [ -d /sys/class/net/$i ]; then
        echo begin /sys/class/net/$i:
        echo name: $i
        for file in address duplex ifindex speed
        do
            path=/sys/class/net/$i/$file
            if [ -r $path ]; then
                value=`cat $path 2>/dev/null`
                if [ "$value" != "" ]; then
                    echo $file: $value
                fi
            fi
        done

        if [ -d /sys/class/net/$i/bonding ]; then
            # Interface is a bonding master
            echo bonded: True
            for file in mode slaves
            do
                path=/sys/class/net/$i/bonding/$file
                if [ -r $path ]; then
                    value=`cat $path 2>/dev/null`
                    if [ "$value" != "" ]; then
                        echo bonding_$file: $value
                    fi
                fi
            done
        fi
        echo end /sys/class/net/$i:
    fi

    SUCCESS=1
    if [ "$ETHTOOL" != "" ]; then
        echo begin ethtool-$i:
        PRIV_ETHTOOL $ETHTOOL $i 2>/dev/null
        SUCCESS=$?
        echo end ethtool-$i:
    fi
    if [ "$MIITOOL" != "" -a $SUCCESS -ne 0 ]; then
        echo begin mii-tool-$i:
        PRIV_MIITOOL $MIITOOL -v $i 2>/dev/null
        echo end mii-tool-$i:
    fi
done
echo 'end details:'
fi

echo --- END ifconfig_if

# getNetworkConnectionList
echo --- START netstat

PRIV_NETSTAT netstat -aneep --tcp --udp -W 2>/dev/null
if [ $? -eq 4 ]; then
    # netstat failed due to invalid option, try -T
    PRIV_NETSTAT netstat -aneep --tcp --udp -T 2>/dev/null
    if [ $? -eq 4 ]; then
        # netstat still failed, try without any wide option
        PRIV_NETSTAT netstat -aneep --tcp --udp 2>/dev/null
    fi
fi

echo --- END netstat

echo --- START ss

PRIV_SS ss -aneptu 2>/dev/null

echo --- END ss

# getProcessList
echo --- START ps
ps -eo pid,ppid,uid,user,cmd --no-headers -ww 2>/dev/null

echo --- END ps

# getPatchList
#   ** UNSUPPORTED **

# getProcessToConnectionMapping
echo --- START lsof-i
PRIV_LSOF lsof -l -n -P -F ptPTn -i 2>/dev/null
echo --- END lsof-i

# getPackageList
echo --- START rpmx
rpm -qa --queryformat 'begin\nname: %{NAME}\nversion: %{VERSION}\nrelease: %{RELEASE}\narch: %{ARCH}\nvendor: %{VENDOR}\nepoch: %{EPOCH}\ndescription: %{SUMMARY}\nend\n' 2>/dev/null

echo --- END rpmx

echo --- START rpm
rpm -qa --queryformat %{NAME}:%{VERSION}:%{RELEASE}:%{ARCH}:%{EPOCH}@ 2>/dev/null

echo
echo --- END rpm

echo --- START dpkg
COLUMNS=256 dpkg -l '*' 2>/dev/null | egrep '^ii '

echo --- END dpkg

# getHBAList
echo --- START hba_sysfs
echo begin sysfs_hba:
if [ ! -d /sys/class ]; then
    echo ERROR: Linux kernel does not support sysfs
elif [ -r /proc/vmware/version ] &&
     [ `grep -o -m1 ESX /proc/vmware/version` ] ||
     [ -r /etc/vmware-release ] &&
     [ `grep -o ESX /etc/vmware-release` ]; then
    echo ERROR: ESX HBA drivers do not support sysfs
elif [ -d /sys/class/fc_host ]; then
    for device in `ls /sys/class/fc_host 2>/dev/null`
    do
        if [ -x /usr/bin/systool ]; then
            systool -c fc_host -v $device
            systool -c scsi_host -v $device
        else
            # systool not present? We will have to,
            # find information from /sys/class/fc_host and /sys/class/scsi_host
            # similar to systool.
            
            # some files are ignored as they do not have read permissions,
            # and does not contain relevant data either.
            ignore_files=( "issue_lip" "vport_create" "vport_delete" "host_reset" 
                           "scan" "trace" "lpfc_xlane_lun" "lpfc_xlane_lun_status"
                           "issue_reset" "ctlreg" "lpfc_aer_state_cleanup" 
                           "lpfc_drvr_stat_data" "lpfc_soft_wwn_enable"
                         )
            device_path_fc="/sys/class/fc_host/$device/"
            # Resolve symlink as we need to identify source device path.
            device_class_path_fc=`readlink -f $device_path_fc`
            echo -e "\nClass = " '"fc_host"'
            echo -e "\n Class Device = " \"$device\"
            echo -e " Class Device path = " \"$device_class_path_fc\"
            
            for f in `ls $device_path_fc 2>/dev/null`
            do
                if [ ! -d $device_path_fc$f ] && [[ ! " ${ignore_files[@]} " =~ " ${f} " ]]; then
                    echo -e "\t$f =" \"`cat $device_path_fc$f 2>/dev/null`\"
                fi
            done
            
            echo -e "\n\tDevice = " \"$device\"
            echo -e "\tDevice path = " \"`echo $device_class_path_fc | sed 's/\/[^/]*\/[^/]*$//'`\"
            
            device_path_scsi="/sys/class/scsi_host/$device/"
            # Resolve symlink as we need to identify source device path.
            device_class_path_scsi=`readlink -f $device_path_scsi`
            echo -e "\n Class = " '"scsi_host"'
            echo -e "\n Class Device = " \"$device\"
            echo -e " Class Device path = " \"$device_class_path_scsi\"
            
            for scsi_f in `ls $device_path_scsi 2>/dev/null`
            do
                if [ ! -d $device_path_scsi$scsi_f ] && [[ ! " ${ignore_files[@]} " =~ " ${scsi_f} " ]]; then
                    echo -e "\t$scsi_f =" \"`cat $device_path_scsi$scsi_f 2>/dev/null`\"
                fi
            done
            
            echo -e "\n\tDevice = " \"$device\"
            echo -e "\tDevice path = " \"`echo $device_class_path_scsi | sed 's/\/[^/]*\/[^/]*$//'`\"
        fi
    done
else
   echo ERROR: Missing /sys/class/fc_host
fi
echo end sysfs_hba:

echo --- END hba_sysfs

echo --- START hba_procfs
echo begin procfs_hba:
if [ ! -d /proc/scsi ]; then
    echo /proc/scsi does not exist
else
    for driver in 'qla*' 'lpfc*'
    do
        for device in `find /proc/scsi/$driver/* 2>/dev/null`
        do
            echo HBA Port $device
            cat $device
        done
    done
fi
echo end procfs_hba:

echo --- END hba_procfs

echo --- START hba_hbacmd

PATH=/usr/sbin/hbanyware:$PATH

echo begin hbacmd_listhbas:
PRIV_HBACMD hbacmd ListHBAs
echo end hbacmd_listhbas:

echo begin hbacmd_hbaattr:
for WWPN in `PRIV_HBACMD hbacmd ListHBAs 2>/dev/null | awk '/Port WWN/ {print $4;}'`
do
    PRIV_HBACMD hbacmd HBAAttrib $WWPN 2>/dev/null
done
echo end hbacmd_hbaattr:

echo --- END hba_hbacmd

# getServices
#   ** UNSUPPORTED **

# getFileSystems
echo --- START df
echo begin df:
PRIV_DF df -lk 2>/dev/null
echo end df:
echo begin mount:
mount 2>/dev/null
echo end mount:
echo begin xtab:
if [ -s /var/lib/nfs/xtab ]; then
    cat /var/lib/nfs/xtab
else
    if [ -s /var/lib/nfs/etab ]; then
        cat /var/lib/nfs/etab
    fi
fi
echo end xtab:
echo begin smbclient:
smbclient -N -L localhost
echo end smbclient:
echo begin smbconf:
configfile=`smbstatus -v 2>/dev/null | grep "using configfile" | awk '{print $4;}'`
if [ "$configfile" != "" ]; then
    if [ -r $configfile ]; then
        cat $configfile
    fi
fi
echo end smbconf:

echo --- END df

